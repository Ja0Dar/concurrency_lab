import Buffer.{Get, Put}
import Commands.{ConsumeDone, ProduceDone}
import akka.actor.{Actor, Stash}
import akka.event.LoggingReceive

object Buffer {

  case class Put(x: Long)

  case object Get

}

class Buffer(n: Int) extends Actor with Stash {


  private val buf = new Array[Long](n)
  private var count = 0

  def receive = LoggingReceive {
    case Put(x) if count < n =>
      unstashAll()
      buf.update(count, x)
      sender() ! ProduceDone
      count += 1
    case Put(x) =>
      stash()
    case Get if count > 0 =>
      unstashAll()
      count -= 1
      val rs = buf(count)
      sender() ! ConsumeDone(rs)
    case Get if count == 0 =>
      stash()
  }
}

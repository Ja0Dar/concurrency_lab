import Buffer.Put
import Commands.{ConsumeDone, Init, ProduceDone}
import akka.actor.{Actor, ActorLogging, ActorRef}

import scala.util.Random

class Consumer(name: String, buf: ActorRef, n:Int) extends Actor with  ActorLogging{


  def receive:Receive = {
    case Init =>
      (1  to n).foreach(_=> buf ! Put(Random.nextInt(100)))
    case ConsumeDone(x)=>
      log.debug(s"cons $name received $x ")
  }
}

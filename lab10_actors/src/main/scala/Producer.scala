import Buffer.Put
import Commands.{Init, ProduceDone}
import akka.actor.{Actor, ActorLogging, ActorRef}

import scala.util.Random

class Producer(name: String, buf: ActorRef, n:Int) extends Actor  with ActorLogging{



  def receive:Receive = {
    case Init =>
      (1  to n).foreach(_=> buf ! Put(Random.nextInt(100)))
    case ProduceDone=>
      log.debug(s"producer $name has buffered ")
  }

}

import Commands.Init
import akka.actor.{ActorSystem, Props}

import scala.concurrent.Await
import scala.concurrent.duration._

// Assignment:
// - implement solution to the producers/consumers problem
//   using the actor model / Akka
// - test the correctness of the created solution for multiple
//   producers and consumers
// Hint: use akka.actor.Stash

// object PC contains messages for all actors -- add new if you need them
object Commands {

  case object Init

  case object ProduceDone

  case class ConsumeDone(x: Long)

}


object ProdConsMain extends App {

  val system = ActorSystem("ProdKons")

  val producers = 10
  val consumers = 4

  val n = 30

  val buf = system.actorOf(Props(new Buffer(n)))

  (1 to producers).foreach(x => {
    val prod = system.actorOf(Props(new Producer(s"producer $x", buf, n)))
    prod ! Init
  })
  (1 to consumers).foreach(x => {
    val cons = system.actorOf(Props(new Consumer(s"producer $x", buf, n)))
    cons ! Init
  })


  Await.result(system.whenTerminated, Duration.Inf)
}





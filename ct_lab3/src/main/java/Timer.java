public class Timer {
    private  long startTime;
    private  long endTime;

    public Timer() {
        startTime=endTime=-1;
    }

    public void  start(){
        endTime=-1;
        startTime= System.nanoTime();
    }

    public void stop(){
        endTime= System.nanoTime();
        if (startTime<0)
           throw new IllegalStateException("Timer wasn't started.");
    }

    public long  getTimeNanoSec(){
        if (startTime<0 || endTime<0)
            throw new IllegalStateException("Timer wasn't started or stopped properly.");
        return endTime-startTime;
    }

    public double getTimeMicroSec(){
        return getTimeNanoSec()/1e3;
    }
    public double getTimeMiliSec(){
        return getTimeNanoSec()/1e6;
    }
    public double getTimeSec(){
        return getTimeNanoSec()/1e9;
    }
}

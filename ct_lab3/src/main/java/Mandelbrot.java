import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import javax.swing.JFrame;

import static java.lang.Integer.min;


public class Mandelbrot extends JFrame {

    private int maxIter;
    private final double ZOOM = 150;
    private final BufferedImage I;
    private int numberOfPacks;
    private ExecutorService executor;

    public Mandelbrot() throws InterruptedException {
        this(570, 5, Executors.newSingleThreadExecutor());

    }

    public Mandelbrot(int max_iter, int numberOfPacks, ExecutorService executor) throws InterruptedException {
        super("Mandelbrot Set");

        maxIter = max_iter;
        this.numberOfPacks = numberOfPacks;
        this.executor = executor;
        setBounds(100, 100, 800, 600);
        setResizable(false);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        I = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

    }

    public void run() throws InterruptedException {
        int range = getHeight() / numberOfPacks;
        for (int y = 0; y < getHeight(); y += range) {
            final int finalY = y;
            executor.execute(() ->
                    execute(finalY, getHeight(), range));
        }
        executor.shutdown();
        executor.awaitTermination(20, TimeUnit.SECONDS);
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(I, 0, 0, this);
    }

    public int execute(int y0, int maxY, int range) {
        double zx, zy, cX, cY, tmp;
        for (int y = y0; y < min(y0 + range, maxY); y++) {
            for (int x = 0; x < getWidth(); x++) {

                zx = zy = 0;
                cX = (x - 400) / ZOOM;
                cY = (y - 300) / ZOOM;
                int iter = maxIter;
                while (zx * zx + zy * zy < 4 && iter > 0) {
                    tmp = zx * zx - zy * zy + cX;
                    zy = 2.0 * zx * zy + cY;
                    zx = tmp;
                    iter--;
                }
                synchronized (I) {
                    I.setRGB(x, y, iter | (iter << 8));
                }
            }
        }
        return 1;
    }


    public static void main(String[] args) throws InterruptedException, IOException {
        final int MAX_ITER = 500;
        List<Supplier<ExecutorService>> executors = Arrays.asList(
                Executors::newSingleThreadExecutor,
                () -> Executors.newFixedThreadPool(3),
                () -> Executors.newFixedThreadPool(30),
                () -> Executors.newFixedThreadPool(90),
                () -> Executors.newFixedThreadPool(300),
                Executors::newWorkStealingPool,
                Executors::newCachedThreadPool);
        List<String> execurorLabes = Arrays.asList(
                "SingleThread",
                "FixedThread(3)",
                "FixedThread(30)",
                "FixedThread(90)",
                "FixedThread(300)",
                "WorkStealingPool",
                "CachedThreadPool"
        );
        List<Integer> packetNr = Arrays.asList(3, 10, 30, 90, 300);

        if (executors.size() != execurorLabes.size()) {
            System.out.println("Fix labels");
            System.exit(-1);
        }

        final Timer t = new Timer();
        FileWriter fw = new FileWriter("a.csv");
        BufferedWriter bw = new BufferedWriter(fw);

        bw.write("Executor");
        for (Integer pNr : packetNr)
            bw.write(String.format(",%s", pNr));
        bw.write("\n");

        for (int i : IntStream.range(0, executors.size()).toArray()) {
            bw.write(execurorLabes.get(i));
            for (Integer ps :
                    packetNr) {
                Mandelbrot mb = new Mandelbrot(MAX_ITER, ps, executors.get(i).get());
                t.start();
                mb.run();
                t.stop();
                bw.write(String.format(",%d", t.getTimeNanoSec()));
            }
            bw.write("\n");


        }
        bw.close();
    }
}

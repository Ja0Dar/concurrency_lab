package pl.agh.edu.tw.webcrawler

import java.net.URL

case class Page(url: URL, html: String)

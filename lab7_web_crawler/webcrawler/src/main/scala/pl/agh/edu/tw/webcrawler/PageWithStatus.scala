package pl.agh.edu.tw.webcrawler

import java.net.URL

import scala.concurrent.Future

case class PageWithStatus(url: URL, html: String, downloaded: Future[Unit]) {
  def toPage = Page(url, html)
}

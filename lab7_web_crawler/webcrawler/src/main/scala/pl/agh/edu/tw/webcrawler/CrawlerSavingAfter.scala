package pl.agh.edu.tw.webcrawler

import java.io.File
import java.net.URL

import com.typesafe.scalalogging.StrictLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.io.{Codec, Source}

object CrawlerSavingAfter extends App with StrictLogging {
  implicit val codec: Codec = Codec.ISO8859 // for Source

  def getSources_BFS(url: URL, depth: Int, maxDepth: Int): Future[Set[Page]] =
    Future(Source.fromURL(url).mkString).
      flatMap { html =>

        if (depth < maxDepth) {
          getHTMLChildren(Page(url, html), depth, maxDepth).map(set => set + Page(url, html))
        } else {
          logger.debug(s"End: ${url.toString}")
          Future.successful(Set(Page(url, html)))
        }

      }.recover {
      case ex: Throwable =>
        logger.error(url.toString)
        logger.error(ex.getMessage)
        ex.printStackTrace()
        Set()
    }


  def getHTMLChildren(parent: Page, depth: Int, maxDepth: Int): Future[Set[Page]] = {
    Future.sequence(
      Auxiliary.links_from_page(parent)
        .map(url => getSources_BFS(url, depth + 1, maxDepth))
    ).map(l => l.flatten)
  }


  def saveFuturePageSet(fPageSet: Future[Set[Page]], dir: String = "html"): Future[Set[Unit]] = {
    fPageSet.flatMap(pageSet =>
      Future.sequence[Unit, Set](
        pageSet.map(page => Auxiliary.savePage(page, dir).recover {
          case ex =>
            logger.warn(s"Couldn't save ${page.url.toString}")
            logger.warn(ex.getMessage)
            ()
        })
      )
    )
  }


  val url = "http://galaxy.agh.edu.pl/~balis/dydakt/tw/"
  val url2 = "http://google.com"

  new File("html").mkdir()
  val futurePages = getSources_BFS(new URL(url), 0, 1)
  val res = Await.result(saveFuturePageSet(futurePages), 30 minutes)

}

package pl.agh.edu.tw.webcrawler

import java.io.FileWriter
import java.net.URL
import java.nio.file.Paths

import com.typesafe.scalalogging.StrictLogging
import org.htmlcleaner.{HtmlCleaner, TagNode}

import scala.concurrent.Future
import scala.io.Codec
import scala.util.{Random, Try}
import concurrent.ExecutionContext.Implicits.global

object Auxiliary extends StrictLogging {
  implicit val codec: Codec = Codec.ISO8859
  val cleaner = new HtmlCleaner

  def tagToUrl(tag: TagNode, parentUrl: URL): URL = {
    val relativeUrl = tag.getAttributeByName("href").mkString
    new URL(parentUrl, relativeUrl)
  }

  def urlToFilename(url: URL): String = {
    Try(Paths.get(url.toURI.getPath).getFileName).toOption
      .map(path => path + "_" + Random.alphanumeric.take(1).mkString)
      .getOrElse(Random.alphanumeric.take(10).mkString)
  }

  def links_from_page(page: Page): Set[URL] =
    cleaner.clean(page.html).getElementsByName("a", true).toSet
      .map(Auxiliary.tagToUrl(_: TagNode, page.url))

  def savePage(page: Page, dir: String = "html") = Future {
    val name = Auxiliary.urlToFilename(page.url)
    val fw = new FileWriter(s"$dir/$name.html")
    fw.write(page.html)
    fw.close()
    logger.debug(s"Saved ${page.url.toString} as $name.html")
    ()
  }
}

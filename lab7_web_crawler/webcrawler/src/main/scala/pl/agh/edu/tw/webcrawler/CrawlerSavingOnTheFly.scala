package pl.agh.edu.tw.webcrawler

import java.io.File
import java.net.URL

import com.typesafe.scalalogging.StrictLogging

import concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Await, Future}
import scala.io.{Codec, Source}

object CrawlerSavingOnTheFly extends App with StrictLogging {
  implicit val codec: Codec = Codec.ISO8859 // for Source

  def getSources_BFS(url: URL, depth: Int, maxDepth: Int): Future[Set[PageWithStatus]] =
    Future(Source.fromURL(url).mkString).
      flatMap { html =>

        val futSave = Auxiliary.savePage(Page(url, html))

        if (depth < maxDepth) {
          getHTMLChildren(Page(url, html), depth, maxDepth).map(set => set + PageWithStatus(url, html, futSave))
        } else {
          logger.debug(s"End: ${url.toString}")
          Future.successful(Set(PageWithStatus(url, html, futSave)))
        }

      }.recover {
      case ex: Throwable =>
        logger.error(url.toString)
        logger.error(ex.getMessage)
        ex.printStackTrace()
        Set()
    }


  def getHTMLChildren(parent: Page, depth: Int, maxDepth: Int): Future[Set[PageWithStatus]] = {
    Future.sequence(
      Auxiliary.links_from_page(parent)
        .map(url => getSources_BFS(url, depth + 1, maxDepth))
    ).map(l => l.flatten)
  }

  val url = "http://galaxy.agh.edu.pl/~balis/dydakt/tw/"
  val url2 = "http://google.com"

  new File("html").mkdir()
  val futurePageWithStatuss = getSources_BFS(new URL(url), 0, 1)
    .flatMap(set => Future.sequence(
      set.map(x => x.downloaded)
    ))
  val res = Await.result(futurePageWithStatuss, 30 minutes)

}

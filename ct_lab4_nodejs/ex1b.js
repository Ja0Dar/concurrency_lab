const async = require('async');

function printAsync(s, cb) {
   var delay = Math.floor((Math.random()*1000)+500);
   setTimeout(function() {
       console.log(s);
       if (cb) cb();
   }, delay);
}

function task1(cb) {
    printAsync("1", function() {
        task2(cb);
    });
    return cb;
}

function task2(cb) {
    printAsync("2", function() {
        task3(cb);
    });
}

function task3(cb) {
    printAsync("3", cb);
}


var a = [
  (cb)=>{
	console.log("lambda1");
	task1(cb);
  },
  (cb)=>{
	console.log("lambda2");
	task1(cb);
	
  },
  (cb)=>{
	console.log("lambda3");
	task1(cb);
  }
];
function end(error,success){
	if(error){console.log("Error")}
	else{console.log("Success")}
}
async.waterfall(a,end);

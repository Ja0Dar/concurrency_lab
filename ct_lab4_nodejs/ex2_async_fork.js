const file = require("file");
const fs = require("fs");
var filename = "ex1b.js";
var count = 0;

var countFile = filename => {
  return callback =>
    fs
      .createReadStream(filename)
      .on("data", function(chunk) {
        count +=
          chunk.toString("utf8").split(/\r\n|[\n\r\u0085\u2028\u2029]/g)
            .length - 1;
      })
      .on("end", function() {
        // console.log(filename, count);
        callback();
      })
      .on("error", function(err) {
        console.error(err);
      });
};

var callbacks = [];

function walkCallback(dirPath, dirs, files) {
  callbacks = callbacks.concat(files.map(fn => file.path.join(dirPath, fn)));
}

file.walkSync("PAM08", walkCallback);

callbacks = callbacks.map(fn => countFile(fn));

var callbackCount = callbacks.length;

var shared_callback = () => {
  callbackCount--;
  if (callbackCount <= 0) {
    console.log(count);
  }
};

callbacks.map(cb => cb(shared_callback));

public class Counter {
    private int counter;

    public synchronized int getCounter() {
        return counter;
    }

    public Counter() {
        this.counter = 0;
    }
    public  synchronized  void increase(){
        this.counter+=1;
    }
    public  synchronized void  decrease(){
        this.counter-=1;
    }
}

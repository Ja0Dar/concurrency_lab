public class PrinterRunnable implements Runnable{
    private int myNumber, nextNumber;
    private IntBox n;

    public PrinterRunnable(int myNumber, int nextNumber, IntBox n) {
        this.myNumber = myNumber;
        this.nextNumber = nextNumber;
        this.n=n;
    }

    @Override
    public void run() {
        synchronized (n) {
            while (true) {
                try {
                    if (n.getValue() == myNumber) {
                        System.out.println(myNumber);
                        if (myNumber==3)
                            System.out.println("");
                        n.setValue(nextNumber);
                        n.notifyAll();
                        n.wait();
                    } else {
                        n.wait();
                    }

                } catch (Exception ex) {
                }
            }
        }
    }
}

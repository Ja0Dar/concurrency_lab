import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws Exception{
        zad2();
    }

    public static void zad1() throws InterruptedException {

        Counter c = new Counter();

        Thread t1 = new Thread(new RunnableChanger(c, true));
        Thread t2 = new Thread(new RunnableChanger(c, false));

        t1.start();
        t2.start();

        t1.join();
        t2.join();
        System.out.println(c.getCounter());
        System.out.println(String.format("err = %f", Math.abs(c.getCounter()) / 1e9));
    }

    public static void zad2() throws InterruptedException {
        Thread t1, t2, t3;


        final IntBox n = new IntBox();
        n.setValue(1);
        t1 = new Thread(new PrinterRunnable(1,2,n));
        t2 = new Thread(new PrinterRunnable(2,3,n));
        t3 = new Thread(new PrinterRunnable(3,1,n));



        t1.start();
        t2.start();
        t3.start();
    }
}



public class RunnableChanger implements Runnable{
    private Counter c;
    boolean decrease;
    public RunnableChanger(Counter c ,boolean decrease) {
        this.c =c;
        this.decrease= decrease;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1e9; i++) {
            if(decrease)c.decrease();
            else c.increase();
        }
    }
}

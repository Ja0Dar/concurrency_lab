// Teoria Współbieżnośi, implementacja problemu 5 filozofów w node.js
// Opis problemu: http://en.wikipedia.org/wiki/Dining_philosophers_problem
// 1. Dokończ implementację funkcji podnoszenia widelca (Fork.acquire).
// 2. Zaimplementuj "naiwny" algorytm (każdy filozof podnosi najpierw lewy, potem
//    prawy widelec, itd.).
// 3. Zaimplementuj rozwiązanie asymetryczne: filozofowie z nieparzystym numerem
//    najpierw podnoszą widelec lewy, z parzystym -- prawy.
// 4. Zaimplementuj rozwiązanie z kelnerem (opisane jako "Arbitrator solution"
//    w wikipedii).
// 5. Zaimplementuj rozwiążanie z jednoczesnym podnoszeniem widelców:
//    filozof albo podnosi jednocześnie oba widelce, albo żadnego.
// 6. Uruchom eksperymenty dla różnej liczby filozofów i dla każdego wariantu
//    implementacji zmierz średni czas oczekiwania każdego filozofa na dostęp
//    do widelców. Wyniki przedstaw na wykresach.
var waitBEB = (n, callback) =>
    setTimeout(callback(2 * n), n)

var Fork = function() {
    this.state = 0;
    return this;
}

Fork.prototype.acquire = function(cb) { //there was callback passed

    var f = (n) => {
        return () => {
            if (this.state == 1) {
                waitBEB(n, f)
            } else {
                this.state = 1;
                cb();
            }
        }
    }
    waitBEB(1, f)
    // zaimplementuj funkcję acquire, tak by korzystala z algorytmu BEB
    // (http://pl.wikipedia.org/wiki/Binary_Exponential_Backoff), tzn:
    // 1. przed pierwszą próbą podniesienia widelca Filozof odczekuje 1ms
    // 2. gdy próba jest nieudana, zwiększa czas oczekiwania dwukrotnie
    //    i ponawia próbę, itd.
}

Fork.prototype.release = function() {
    this.state = 0;
}

var Philosopher = function(id, forks) {
    this.id = id;
    this.forks = forks;
    this.f1 = id % forks.length;
    this.f2 = (id + 1) % forks.length;
    return this;
}

Philosopher.prototype.startNaive = function(count, print) {
    var forks = this.forks,
        f1 = this.f1,
        f2 = this.f2,
        id = this.id

    var takeLeftRight = (n) => {
        var counter = 0;
        forks[f1].acquire(() => {
            if (print) {
                console.log(id + " took " + f1);
            }
            forks[f2].acquire(() => {
                if (print) {
                    console.log(id + " took " + f2);
                    console.log(id + "eats and releases " + f1 + " " + f2);
                }
                forks[f1].release();
                forks[f2].release();
                if (n > 0) {
                    takeLeftRight(n - 1);
                }
            });
        });
    };
    takeLeftRight(count);

    // zaimplementuj rozwiązanie naiwne
    // każdy filozof powinien 'count' razy wykonywać cykl
    // podnoszenia widelców -- jedzenia -- zwalniania widelców
}

Philosopher.prototype.startAsym = function(count, print) {
    var forks = this.forks,
        id = this.id


    if (id % 2 == 0) {
        var f1 = this.f1;
        var f2 = this.f2;
    } else {
        var f2=this.f1
        var f1=this.f2
    }
    var takeForks= (n) => {
        var counter = 0;
        forks[f1].acquire(() => {
            if (print) {
                console.log(id + " took " + f1);
            }
            forks[f2].acquire(() => {
                if (print) {
                    console.log(id + " took " + f2);
                    console.log(id + "eats and releases " + f1 + " " + f2);
                }
                forks[f1].release();
                forks[f2].release();
                if (n > 0) {
                    takeForks(n - 1);
                }
            });
        });
    };


takeForks(count);
    // zaimplementuj rozwiązanie asymetryczne
    // każdy filozof powinien 'count' razy wykonywać cykl
    // podnoszenia widelców -- jedzenia -- zwalniania widelców
}


var Waiter= function(N) {
    this.state = 0;
    this.N=N;
    return this;
}


Waiter.prototype.acquire = function(cb) { //there was callback passed

    var f = (n) => {
        return () => {
            if (this.state >= this.N-1) {
                waitBEB(n, f)
            } else {
                this.state +=1;
                cb();
            }
        }
    }
    waitBEB(1, f)
    // zaimplementuj funkcję acquire, tak by korzystala z algorytmu BEB
    // (http://pl.wikipedia.org/wiki/Binary_Exponential_Backoff), tzn:
    // 1. przed pierwszą próbą podniesienia widelca Filozof odczekuje 1ms
    // 2. gdy próba jest nieudana, zwiększa czas oczekiwania dwukrotnie
    //    i ponawia próbę, itd.
}

Waiter.prototype.release = function() {
    this.state -=1;
}

Philosopher.prototype.startConductor = function(count, waiter, print) {
    var forks = this.forks,
        f1 = this.f1,
        f2 = this.f2,
        id = this.id

    var takeLeftRight = (n) => {
        var counter = 0;
        forks[f1].acquire(() => {
            if (print) {
                console.log(id + " took " + f1);
            }
            forks[f2].acquire(() => {
                waiter.release();
                if (print) {
                    console.log(id + " took " + f2);
                    console.log(id + "eats and releases " + f1 + " " + f2 + " and waiter");
                }
                forks[f1].release();
                forks[f2].release();
                if (n > 0) {
                    waiter.acquire(() => takeLeftRight(n - 1));
                }
            });
        });
    };

    waiter.acquire(() => {
        if (print) {
            console.log(id + " asked waiter");
        }
        takeLeftRight(count);
    });


    // zaimplementuj rozwiązanie z kelnerem
    // każdy filozof powinien 'count' razy wykonywać cykl
    // podnoszenia widelców -- jedzenia -- zwalniania widelców
};

Philosopher.prototype.pickBothForks = function(cb) {
    var forks = this.forks,
        f1 = this.f1,
        f2 = this.f2;


    var f = (n) => {
        return () => {
            if (forks[f1].state == 1 || forks[f2].state == 1) {
                waitBEB(n, f)
            } else {
                forks[f1].state = 1;
                forks[f2].state = 1;
                cb();
            }
        }
    }
    waitBEB(1, f)
}


Philosopher.prototype.startPickBoth = function(count, print) {
    var forks = this.forks,
        f1 = this.f1,
        f2 = this.f2,
        id = this.id
    var tryToEat = (n) =>
        this.pickBothForks(() => {
            if (print) {
                console.log(id + " picked up " + f1 + " and " + f2);
                console.log(id + " ate and released " + f1 + " and " + f2);
            }
            process.nextTick(()=>{


            forks[f1].release();
            forks[f2].release();

            if (n > 0) {
                tryToEat(n - 1);
            }


            });
        });
    tryToEat(count);

}
// wersja z jednoczesnym podnoszeniem widelców
// Algorytm BEB powinien obejmować podnoszenie obu widelców,
// a nie każdego z osobna

var runPhilosophers = function(type,philNumber,meals, print) {
    var N = philNumber;//5

    var forks = [];
    var philosophers = []
    for (var i = 0; i < N; i++) {
        forks.push(new Fork());
    }

    for (var i = 0; i < philNumber; i++) {
        philosophers.push(new Philosopher(i, forks));
    }

    switch (type) {
        case "naive":
            philosophers.forEach(phil => phil.startNaive(meals, print));
            break;
        case "asym":
            philosophers.forEach(phil => phil.startAsym(meals, print));
            break;
        case "waiter":
            waiter = new Waiter(N);
            philosophers.forEach(phil => phil.startConductor(meals,waiter, print));
            break;
        case "pickboth":
            philosophers.forEach(phil => phil.startPickBoth(meals, print));
            break;
        default:
        console.log("Wrong type");

    }
}

type= process.argv[2];
if (type =="--help" || type =="help"){
  console.log("\n"+
   "node scriptname.js type meals print\n\n"+
   "    type = naive|asym|waiter|pickboth\n"+
   "    philosophers - number  of philosophers\n"+
   "    meals - number  of meals to eat for each phil\n"+
   "    print = 0|1  // whether you want things to be printed\n\n");
}else{
  philNumber=Number(process.argv[3])
  meals = Number(process.argv[4]);
  print= Boolean(Number(process.argv[5]));
  runPhilosophers(type,philNumber,meals,print);
}

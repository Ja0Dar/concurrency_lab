package prodcon;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class AsyncFileWriter {
    BufferedWriter bf;
    ExecutorService exec;


    public AsyncFileWriter(String path) throws IOException {
        bf = new BufferedWriter(new FileWriter(path));
        exec = Executors.newFixedThreadPool(4);
    }

    public void write(String str) {

        exec.submit(() -> {
            try {
                bf.write(str);
            } catch (IOException ignored) {
            }
        });
    }

    public void close() throws IOException {
        bf.close();
        exec.shutdown();
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }
}

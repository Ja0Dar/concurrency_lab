package prodcon;

import java.io.IOException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class FifoBuffer extends AbstractBuffer {

    private Lock lock;
    private Condition firstProdCond, restProdCond, firstConsCond, restConsCond;
    private boolean firstProdWaits, firstConsWaits;


    public FifoBuffer(int m, String path, long accessLimit) throws IOException {
        super(m, path, accessLimit);

        lock = new ReentrantLock();
        firstProdCond = lock.newCondition();

        restProdCond = lock.newCondition();

        firstConsCond = lock.newCondition();

        restConsCond = lock.newCondition();

        firstProdWaits = false;
        firstConsWaits = false;
    }

    public FifoBuffer(BufferParams bp) throws IOException {
        this(bp.m, bp.s, bp.accessLimit);
    }


    @Override
    public void timeStampedInsert(int numberOfElements) throws IllegalArgumentException, InterruptedException, IOException {
        long startTime = System.nanoTime();
        lock.lock();
        if (firstProdWaits) {
            restProdCond.await();
        }

        while (!canInsert(numberOfElements)) {
            firstProdWaits = true;
            firstProdCond.await();
        }

        firstProdWaits = false;
        state += numberOfElements;

        restProdCond.signal();
        firstConsCond.signal();

        incAccessCounter();
        long endTime = System.nanoTime();
        afw.write(String.format("%d %d\n", numberOfElements, endTime - startTime));
        lock.unlock();

    }

    @Override
    public void timeStampedWithdraw(int numberOfElements) throws IllegalArgumentException, InterruptedException, IOException {
        long startTime = System.nanoTime();

        lock.lock();

        if (firstConsWaits){
            restConsCond.await();
        }

        while (!canWithdraw(numberOfElements)) {
            firstConsWaits = true;
            firstConsCond.await();

        }
        firstConsWaits = false;
        state -= numberOfElements;

        restConsCond.signal();
        firstProdCond.signal();
        incAccessCounter();
        long endTime = System.nanoTime();
        afw.write(String.format("%d %d\n", numberOfElements, endTime - startTime));
        lock.unlock();
    }
}

package prodcon;

import prodcon.myrandom.RandomIntGenerator;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class Producer implements Runnable {

    private AbstractBuffer buff;
    private RandomIntGenerator rig;
    public Producer(AbstractBuffer buffer, RandomIntGenerator rig) {
        this.buff = buffer;
        this.rig = rig;
    }

    @Override
    public void run() {
        int m = buff.getM();

        while (!buff.isWornOut() && !Thread.interrupted()) {
            try {
                buff.timeStampedInsert(rig.random(1,m));
            } catch (InterruptedException | IOException e) {
                return;
            }
        }

    }
}

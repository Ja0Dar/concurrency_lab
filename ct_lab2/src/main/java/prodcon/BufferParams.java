package prodcon;

public class BufferParams {
    public final int m;
    final String s;
    final long accessLimit;

    public BufferParams(int m, String path, long accessLimit) {
        this.m = m;
        this.s = path;
        this.accessLimit = accessLimit;
    }

}

package prodcon.myrandom;

public interface RandomIntGenerator {
    public int random(int minValue,int maxValue);
    static RandomIntGenerator NormalIntGen(){
        return new NormalRandomInt();
    }
    static SkewedToMinRandomGenerator SkewedToMinGen(){
        return new SkewedToMinRandomGenerator();
    }
}

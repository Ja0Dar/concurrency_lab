package prodcon.myrandom;

import java.util.concurrent.ThreadLocalRandom;

public class NormalRandomInt implements RandomIntGenerator {
    @Override
    public int random(int minValue, int maxValue) {
        return ThreadLocalRandom.current().nextInt(minValue, maxValue);
    }
}

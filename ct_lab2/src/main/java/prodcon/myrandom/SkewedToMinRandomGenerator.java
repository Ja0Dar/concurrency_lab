package prodcon.myrandom;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class SkewedToMinRandomGenerator implements RandomIntGenerator {
    private  Random r;
    public SkewedToMinRandomGenerator() {
       r = new Random();
    }

    @Override
    public int random(int minValue, int maxValue) {

        return (int) (Math.abs(r.nextFloat()-r.nextFloat()) * (1 + maxValue - minValue) + minValue);
    }
}

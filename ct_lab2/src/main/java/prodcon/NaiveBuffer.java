package prodcon;


import java.io.IOException;

public class NaiveBuffer extends AbstractBuffer {
    public NaiveBuffer(int m,String path,long accessLimit) throws IOException {
        super(m, path,accessLimit);
    }

    public NaiveBuffer(BufferParams bp) throws IOException {
        super(bp);
    }


    public synchronized void timeStampedInsert(int numberOfElements) throws IllegalArgumentException, InterruptedException, IOException {
        long startTime = System.nanoTime();
        raw_insert(numberOfElements);
        incAccessCounter();
        long endTime = System.nanoTime();
        afw.write(String.format("%d %d\n", numberOfElements, endTime - startTime));
    }

    public synchronized void timeStampedWithdraw(int numberOfElements) throws IllegalArgumentException, InterruptedException, IOException {
        long startTime = System.nanoTime();
        raw_withdraw(numberOfElements);
        incAccessCounter();
        long endTime = System.nanoTime();
        afw.write(String.format("%d %d\n", numberOfElements, endTime - startTime));

    }

    void raw_insert(int numberOfElements) throws IllegalArgumentException, InterruptedException {
        if (numberOfElements > m) {
            throw new IllegalArgumentException("inserting too many");
        }

        while (!canInsert(numberOfElements)) {
            this.wait();
        }

        state += numberOfElements;
        notifyAll();


    }


    void raw_withdraw(int numberOfElements) throws IllegalArgumentException, InterruptedException {
        if (numberOfElements > m) {
            throw new IllegalArgumentException("withdrawing too many");
        }

        while (!canWithdraw(numberOfElements)) {
            this.wait();
        }

        state -= numberOfElements;
        notifyAll();


    }
}

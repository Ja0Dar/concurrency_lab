package prodcon;

import java.io.IOException;

public abstract class AbstractBuffer {

    final int m;
    int state;

    AsyncFileWriter afw;

    private final long accessLimit;

    private long accessCounter;


    public AbstractBuffer(int m, String path, long accessLimit) throws IOException {

        this.m = m;
        this.state = 0;
        afw = new AsyncFileWriter(path);
        this.accessLimit = accessLimit;
        accessCounter = 0;
    }

    public AbstractBuffer(BufferParams bp) throws IOException {
        this(bp.m,bp.s,bp.accessLimit);
    }

    void incAccessCounter() {
        accessCounter += 1;
    }

    public boolean isWornOut() {
        return accessCounter >= accessLimit;
    }

    public abstract void timeStampedInsert(int numberOfElements) throws IllegalArgumentException, InterruptedException, IOException;

    public abstract void timeStampedWithdraw(int numberOfElements) throws IllegalArgumentException, InterruptedException, IOException;

    void soutWithdraw(int nofElems) {
        StringBuilder sb = new StringBuilder("T")
                .append(Thread.currentThread().getName())
                .append("took :::   ")
                .append(nofElems)
                .append("\n ")
                .append(state + nofElems)
                .append("->")
                .append(state)
                .append("\n");

        System.out.println(sb.toString());

    }

    void soutInsert(int nofElems) {
        StringBuilder sb = new StringBuilder("T")
                .append(Thread.currentThread().getName())
                .append("inserted  :::  ")
                .append(nofElems)
                .append("\n ")
                .append(state - nofElems)
                .append("->")
                .append(state)
                .append("\n");

        System.out.println(sb.toString());
    }

    boolean canInsert(int elementsToInsert) {
        return freeSpots() >= elementsToInsert;
    }

    boolean canWithdraw(int elementsToWithdraw) {
        return state >= elementsToWithdraw;
    }


    int freeSpots() {
        return 2 * m - state;
    }

    public int getM() {
        return m;
    }


    public long getAccessLimit() {
        return accessLimit;
    }

    public void closeLog() throws IOException {
        afw.close();
    }

    public int getState() {
        return state;
    }
}

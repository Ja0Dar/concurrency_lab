package prodcon;

import prodcon.myrandom.RandomIntGenerator;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class Consumer implements Runnable {

    private AbstractBuffer buff;
    private RandomIntGenerator rig;

    public Consumer(AbstractBuffer buffer, RandomIntGenerator rig) {
        this.buff = buffer;
        this.rig = rig;
    }

    @Override
    public void run() {
        int m = buff.getM();

        try {
            while (!buff.isWornOut() && !Thread.interrupted()) {
                buff.timeStampedWithdraw(rig.random(1, m));
            }
        } catch (InterruptedException | IOException e) {
            return;
        }


    }

}

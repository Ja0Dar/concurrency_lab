import prodcon.*;
import prodcon.myrandom.RandomIntGenerator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.Integer.max;

public class Main {
    public static void main(String[] args) throws InterruptedException, IOException, ExecutionException {
        AbstractBuffer b;

        RandomIntGenerator rig = RandomIntGenerator.SkewedToMinGen();
        for (int prodAndConNumber :
                Arrays.asList(10, 100, 1000)
                ) {
            for (double m : Arrays.asList(1e3, 1e4, 1e5)) {

                System.out.println(String.format("m= %d prod = %d started", (int) m, (int) prodAndConNumber));
                b = new NaiveBuffer(
                        (int) m,
                        String.format("naive_m%d_prod%d.dat", (int) m, prodAndConNumber),
                        (long) 1e6
                );

                executeExercise(b, prodAndConNumber, prodAndConNumber, rig);
                b.closeLog();


                b = new FifoBuffer(
                        (int) m,
                        String.format("fifo_m%d_prod%d.dat", (int) m, prodAndConNumber),
                        (long) 1e6
                );
                executeExercise(b, prodAndConNumber, prodAndConNumber, rig);
                b.closeLog();
                System.out.println(String.format("m= %d prod = %d finished.   Active threads = %d", (int) m, (int) prodAndConNumber, Thread.activeCount()));
                System.out.flush();
            }
        }
        System.out.println("Back to main");
        System.exit(1);


    }


    public static void executeExercise(AbstractBuffer buffer, int producers, int consumers, RandomIntGenerator rig) throws ExecutionException, InterruptedException {


        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < max(producers, consumers); i++) {
            if (i <= producers)
                threads.add(new Thread(new Producer(buffer, rig)));
            if (i <= consumers)
                threads.add(new Thread(new Consumer(buffer, rig)));
        }
        final int[] i = {0};
        threads.forEach((Thread::start));


        while (threads.stream().filter(t -> !t.isAlive()).count() <= 0.2 * producers) {
            threads.get(0).join(100);
        }
        threads.forEach(Thread::interrupt);


        threads.forEach(t -> {
            try {
                t.join(100);
            } catch (InterruptedException e) {
                System.out.println("Join interrupt");
                e.printStackTrace();
            }
        });

        threads.forEach(Thread::stop);// todo - how to avoid it?
    }
}

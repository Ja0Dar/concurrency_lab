package semaphores;

public class IntBox {

    private int value;

    public void increment(){
        value+=1;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

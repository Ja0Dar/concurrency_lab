package semaphores;

public class SemaphoreBinary {
    boolean available;

    public SemaphoreBinary() {
        available = true;
    }

    public SemaphoreBinary(boolean availableOnStart) {
        available = availableOnStart;
    }


    public synchronized void take() throws InterruptedException {
        while (!available) {
            this.wait();
        }
        this.available = false;
//        this.notify();
    }

    public synchronized void give() throws InterruptedException {
        if (!available) {
            available = true;
            notify();
        }
    }

    public synchronized void P() {
        try {
            take();
        } catch (InterruptedException ignored) {
        }
    }

    public synchronized void V() {
        try {
            give();
        } catch (InterruptedException ignored) {

        }
    }


    public static void test() throws  InterruptedException {

        final SemaphoreBinary sem = new SemaphoreBinary();
        final int N = 1000000;
        Thread t1, t2;
        IntBox count = new IntBox();
        t1 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                sem.P();
                count.increment();
                sem.V();
            }
        });
        t2 = new Thread(() -> {
            for (int i = 0; i < N; i++) {
                sem.P();
                count.increment();
                sem.V();
            }
        });

        t1.start();
        t2.start();
        t1.join();
        t2.join();

        System.out.println(count.getValue());

    }
}

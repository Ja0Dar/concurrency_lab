def plot(namebase):
    import numpy as np
    import matplotlib.pyplot as plt
    import matplotlib.mlab as mlab
    f, axarr = plt.subplots(3, 3)

    for m_i, m in enumerate(map(int, [1e3, 1e4, 1e5])):
        for prod_i, prod in enumerate([10, 100, 1000]):
            filename = "{}_m{}_prod{}.dat".format(namebase, m, prod)
            name = filename.split('.')[0]
            # raw_X, raw_Y = np.loadtxt(filename, delimiter=' ', usecols=(0, 1), unpack=True)
            data = np.loadtxt(filename, dtype=[('m', np.uint32), ('time', np.uint64)])

            res = mlab.rec_groupby(data, ('m',), (('time', np.mean, 'avg_time'),))

            axarr[m_i, prod_i].scatter(res.m, res.avg_time, s=1)
            axarr[m_i, prod_i].set_title(name)
    # plt.savefig(namebase)
    plt.show()


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('namebase', choices=['naive', 'fifo'], help="naive or fifo")
    args = parser.parse_args()
    plot(args.namebase)
